#Laravel Modular

Description
-----------
This package is developed for better housekeeping of PHP classes, it will help the developer to design a clean file structure of php files and classes. 


Installation
------------

Open you terminal, make sure the composer is installed in you machine and execute the following command to get the latest version of the package:

```shell
composer require cab-php/module-generator
```

In your `config/app.php` add `TeamOptimus\ModuleGenerator\ModuleGeneratorServiceProvider::class` to the end of the `providers` array:


Publish Configuration

```shell
php artisan vendor:publish --provider="TeamOptimus\ModuleGenerator\ModuleGeneratorServiceProvider::class" 
```

File structure sample
---------------------

Please see the following files structure sample.

```shell
app/
	Modules/
		Controllers/
			ControllerClass.php
		Models/
			ModelClass.php
		Requests/
			RequestClass.php
		Repositories/
			RepositoryInterface.php
			RepositoryEloquent.php
		Commands/
			CommandClass.php
		Event/
			EventClass.php
		Listener/
			ListenerClass.php
```

You can also change the Path in the package configuration file.

