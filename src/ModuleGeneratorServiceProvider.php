<?php namespace TeamOptimus\ModuleGenerator;

use Illuminate\Support\ServiceProvider;

class ModuleGeneratorServiceProvider extends ServiceProvider {
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$configPath = __DIR__ . '/config/module_generator.php';
        $this->mergeConfigFrom($configPath, 'module_generator');
        $this->publishes([$configPath => config_path('module_generator.php')], 'config');
        
        $this->commands([
            \TeamOptimus\ModuleGenerator\Console\GenerateModuleCommand::class,
            \TeamOptimus\ModuleGenerator\Console\RequestCommand::class,
            \TeamOptimus\ModuleGenerator\Console\EventCommand::class,
            \TeamOptimus\ModuleGenerator\Console\ListenerCommand::class,
            \TeamOptimus\ModuleGenerator\Console\ControllerCommand::class,
            \TeamOptimus\ModuleGenerator\Console\ProviderCommand::class,
            \TeamOptimus\ModuleGenerator\Console\ModelCommand::class,
            \TeamOptimus\ModuleGenerator\Console\RepositoryCommand::class,
            \TeamOptimus\ModuleGenerator\Console\RepositoryCommand::class,
            \TeamOptimus\ModuleGenerator\Console\JobCommand::class,
            \TeamOptimus\ModuleGenerator\Console\Generators\RepositoryInterfaceGenerator::class,
            \TeamOptimus\ModuleGenerator\Console\Generators\RepositoryGenerator::class,
            \TeamOptimus\ModuleGenerator\Console\MakeCommand::class,
        ]);
	}

	public function boot()
	{
	}
}