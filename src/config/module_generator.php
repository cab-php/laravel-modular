<?php

return [
	'folder' => 'Modules',
    'paths' => [
		'models'       => 'Models',
		'repositories' => 'Repositories',
		'validators'   => 'Validators',
		'controllers'  => 'Controllers',
		'events'       => 'Events',
		'listeners'    => 'Listeners',
		'requests'     => 'Requests',
		'providers'    => 'Providers',
		'jobs'         => 'Jobs',
		'provider'     => 'RepositoryServiceProvider',
		'commands'     => 'Commands',
    ]
];