<?php

namespace TeamOptimus\ModuleGenerator\Console;

use Illuminate\Console\Command;
use TeamOptimus\ModuleGenerator\DetectsApplicationNamespace;

class ControllerCommand extends Command
{
    use DetectsApplicationNamespace;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:module-controller {module} {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Module Controller. Ex. php artisan make:module-controller Login LoginController';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('make:controller', [
            'name' => "\\{$this->getAppNamespace()}".config('module_generator.folder')."\\".$this->argument('module').'\\'.config('module_generator.paths.controllers').'\\'.$this->argument('name'),
            '--resource' => true
        ]);
    }
}