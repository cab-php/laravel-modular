<?php

namespace TeamOptimus\ModuleGenerator\Console;

use Illuminate\Console\Command;
use TeamOptimus\ModuleGenerator\DetectsApplicationNamespace;

class EventCommand extends Command
{
    use DetectsApplicationNamespace;
    
	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:module-event {module} {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Modules Event. Ex. php artisan make:module-event Login TestEvent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('make:event', [
    		'name' => "\\{$this->getAppNamespace()}".config('module_generator.folder')."\\".$this->argument('module').'\\'.config('module_generator.paths.events').'\\'.$this->argument('name')
    	]);
    }
}