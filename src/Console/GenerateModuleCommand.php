<?php

namespace TeamOptimus\ModuleGenerator\Console;

use Illuminate\Console\Command;
use TeamOptimus\ModuleGenerator\DetectsApplicationNamespace;

class GenerateModuleCommand extends Command
{    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:module {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Module. Ex. php artisan make:module Login';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->confirm('Would you like to create a Model? [y|N]')) {
            $this->call('make:module-model', [
                'module' => $this->argument('name'),
                'name'   => $this->argument('name'),
            ]);
        }
        if ($this->confirm('Would you like to create a Controller? [y|N]')) {
            $this->call('make:module-controller', [
                'module' => $this->argument('name'),
                'name'   => $this->argument('name').'Controller',
            ]);
        }
        if ($this->confirm('Would you like to create a Repository? [y|N]')) {
            $this->call('make:module-repository', [
                'module' => $this->argument('name'),
                'name'   => $this->argument('name').'Repository',
            ]);
        }
        if ($this->confirm('Would you like to create a Request Class? [y|N]')) {
            $name = $this->ask('Please enter the request class name');
            $this->call('make:module-request', [
                'module' => $this->argument('name'),
                'name'   => $name,
            ]);
        }
        app('Illuminate\Support\Composer')->dumpOptimized();
    }
}
