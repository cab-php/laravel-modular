<?php

namespace TeamOptimus\ModuleGenerator\Console;

use Illuminate\Console\Command;
use TeamOptimus\ModuleGenerator\DetectsApplicationNamespace;

class ProviderCommand extends Command
{
    use DetectsApplicationNamespace;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:module-provider {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Modules Provider. Ex. php artisan make:module-provider LoginProvider';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

    	$this->call('make:provider', [
    		'name' => "\\{$this->getAppNamespace()}".config('module_generator.folder')."\\".config('module_generator.paths.providers')."\\".$this->argument('name')
    	]);
    }
}
