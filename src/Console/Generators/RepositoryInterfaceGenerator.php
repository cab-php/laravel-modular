<?php

namespace TeamOptimus\ModuleGenerator\Console\Generators;

use Illuminate\Console\Command;
use TeamOptimus\ModuleGenerator\DetectsApplicationNamespace;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;
use TeamOptimus\ModuleGenerator\Console\WithModelStub;
use Illuminate\Support\Str;

class RepositoryInterfaceGenerator extends GeneratorCommand
{
    use DetectsApplicationNamespace, WithModelStub;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:module-repository-interface-generator {module} {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Modules Repository Interface. Ex. php artisan make:module-repository-interface-generator User UserRepository';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Repository Interface';


    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        $stub = $stub ?? '/stubs/repository/interface.stub';

        return __DIR__ . $stub;
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\\'.config('module_generator.folder').'\\'.$this->argument('module').'\\'.config('module_generator.paths.repositories');
    }

    /**
     * Build the class with the given name.
     * Remove the base controller import if we are already in base namespace.
     *
     * @param  string $name
     *
     * @return string
     */
    protected function buildClass($name)
    {
        $replace = [];
        // if ($this->option('model')) {
        //     $replace = $this->buildModelReplacements($replace);
        // }

        return str_replace(
            array_keys($replace), array_values($replace), parent::buildClass($name)
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            // ['model', 'm', InputOption::VALUE_OPTIONAL, 'Generate an export for the given model.'],
            // ['query', '', InputOption::VALUE_NONE, 'Generate an export for a query.'],
        ];
    }

    /**
     * Build the model replacement values.
     *
     * @param  array  $replace
     * @return array
     */
    protected function buildModelReplacements(array $replace)
    {
        $modelClass = $this->parseModel($this->option('model'));

        return array_merge($replace, [
            'DummyFullModelClass' => $modelClass,
            'DummyModelClass' => class_basename($modelClass),
            // 'DummyModelVariable' => lcfirst(class_basename($modelClass)),
        ]);
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        return $this->laravel['path'].'/'.str_replace('\\', '/', $name).'Interface.php';
    }

}
