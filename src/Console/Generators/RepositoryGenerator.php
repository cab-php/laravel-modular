<?php

namespace TeamOptimus\ModuleGenerator\Console\Generators;

use Illuminate\Console\Command;
use TeamOptimus\ModuleGenerator\DetectsApplicationNamespace;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;
use TeamOptimus\ModuleGenerator\Console\WithModelStub;

class RepositoryGenerator extends GeneratorCommand
{
    use DetectsApplicationNamespace, WithModelStub;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:module-repository-generator {module} {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Modules Repository. Ex. php artisan make:module-repository-generator User User';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Repository';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        $stub = $stub ?? '/stubs/repository/repository_class.stub';

        return __DIR__ . $stub;
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\\'.config('module_generator.folder').'\\'.$this->argument('module').'\\'.config('module_generator.paths.repositories');
    }

    /**
     * Build the class with the given name.
     * Remove the base controller import if we are already in base namespace.
     *
     * @param  string $name
     *
     * @return string
     */
    protected function buildClass($name)
    {
        $replace = [];
        // if ($this->option('model')) {
        //     $replace = $this->buildModelReplacements($replace);
        // }

        return str_replace(
            array_keys($replace), array_values($replace), parent::buildClass($name)
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            // ['model', 'm', InputOption::VALUE_OPTIONAL, 'Generate an export for the given model.'],
            // ['query', '', InputOption::VALUE_NONE, 'Generate an export for a query.'],
        ];
    }
}
