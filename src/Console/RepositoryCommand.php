<?php

namespace TeamOptimus\ModuleGenerator\Console;

use Illuminate\Console\Command;
use TeamOptimus\ModuleGenerator\DetectsApplicationNamespace;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;
use TeamOptimus\ModuleGenerator\Console\Generators\RepositoryGenerator;
use TeamOptimus\ModuleGenerator\Console\Generators\RepositoryInterfaceGenerator;

class RepositoryCommand extends Command
{
    use DetectsApplicationNamespace, WithModelStub;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:module-repository {module} {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Modules Repository. Ex. php artisan make:module-repository User User';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Repository';

    /**
     * Execute the command.
     *
     * @see fire()
     * @return void
     */
    public function handle(){
        $this->call('make:module-repository-generator', [
            'module' => $this->argument('module'),
            'name' => $this->argument('name')
        ]);
        $this->call('make:module-repository-interface-generator', [
            'module' => $this->argument('module'),
            'name' => $this->argument('name')
        ]);

        // app('Illuminate\Support\Composer')->dumpOptimized();
        // $this->laravel->call([$this, 'fire'], func_get_args());
    }

    // public function fire()
    // {
    //     // (new RepositoryGenerator([
    //     //     'module' => $this->argument('module'),
    //     //     'name' => $this->argument('name'),
    //     // ]))->run();

    //     (new RepositoryInterfaceGenerator([
    //         'module' => $this->argument('module'),
    //         'name' => $this->argument('name'),
    //     ]))->run();
    // }

}
